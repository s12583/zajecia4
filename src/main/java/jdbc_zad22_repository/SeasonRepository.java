/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import jdbc_zad22_dominanclass.Season;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.EpisodeMapper;
import jdbc_zadv22_mappers.SeasonMapper;
import jdbc_zadv22_unityofwork.IUnitOfWork;


public class SeasonRepository implements Repository<Season> {

    private SeasonMapper seasonMapper;
    private EpisodeMapper episodeMapper;





    public SeasonRepository(Connection connection, IUnitOfWork uow) {
        this.seasonMapper = new SeasonMapper(connection, uow);
        this.episodeMapper = new EpisodeMapper(connection, uow);



    }


    @Override
    public Season GetById(int id) {


        seasonMapper.find((long) id).setEpisodes(episodeMapper.allWithSeasonId(id));

        return seasonMapper.find((long) id);


    }

    @Override
    public List<Season> GetAll()
    {

        return seasonMapper.findAll();
    }

    @Override
    public void save(Season item)
    {
        seasonMapper.save(item);
    }

    @Override
    public void update(Season item)
    {
      seasonMapper.update(item);
   
    }

    @Override
    public void remove(Season item)
    {
        seasonMapper.remove(item);
    }
    
}
