/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import jdbc_zad22_dominanclass.TvSeries;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.SeasonMapper;
import jdbc_zadv22_mappers.TvSeriesMapper;
import jdbc_zadv22_unityofwork.IUnitOfWork;

/**
 *
 * @author Grzegorz
 */
public class TvSeriesRepository implements Repository<TvSeries>
{
    private TvSeriesMapper tvSeriesMapper;
    private SeasonMapper seasonMapper;




    public TvSeriesRepository(Connection connection, IUnitOfWork uow)
    {
        this.tvSeriesMapper = new TvSeriesMapper(connection, uow);
        this.seasonMapper = new SeasonMapper(connection, uow);




    }
    
    

    @Override
    public TvSeries GetById(int id)
    {
        tvSeriesMapper.find((long) id).setSeasons(seasonMapper.allWithTvSeriesId(id));
       return tvSeriesMapper.find((long) id); 
    }

    @Override
    public List<TvSeries> GetAll() {

        return tvSeriesMapper.findAll();
    }

    @Override
    public void save(TvSeries item)
    {
        tvSeriesMapper.save(item);
    }

    @Override
    public void update(TvSeries item)
    {
        tvSeriesMapper.update(item);
    
    }

    @Override
    public void remove(TvSeries item)
    {
        tvSeriesMapper.remove(item);
    }
    
}
