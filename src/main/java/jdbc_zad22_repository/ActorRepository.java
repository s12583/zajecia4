/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import jdbc_zad22_dominanclass.Actor;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.ActorMapper;
import jdbc_zadv22_unityofwork.IUnitOfWork;

/**
 *
 * @author Grzegorz
 */
public class ActorRepository implements Repository<Actor>
{
   
    private ActorMapper actorMapper;



    public ActorRepository(Connection connection, IUnitOfWork uow)
    {
      
        this.actorMapper = new ActorMapper(connection, uow);


    }

    @Override
    public Actor GetById(int id)
    {
        return actorMapper.find((long) id);
    }

    @Override
    public List<Actor> GetAll() {

        return actorMapper.findAll();
    }

    @Override
    public void save(Actor item)
    {
        actorMapper.save(item);
    }

    @Override
    public void update(Actor item)
    {
        actorMapper.update(item);
    }

    @Override
    public void remove(Actor item)
    {
        actorMapper.remove(item);
    }
    
    

    
}
