/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import jdbc_zad22_dominanclass.Director;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.DirectorMapper;
import jdbc_zadv22_unityofwork.IUnitOfWork;

/**
 *
 * @author Grzegorz
 */
public class DirectorRepository implements Repository<Director>
{
    private DirectorMapper directorMapper;



    public DirectorRepository(Connection connection, IUnitOfWork uow)
    {
        this.directorMapper = new DirectorMapper(connection,uow);


    }

    @Override
    public Director GetById(int id)
    {
        return directorMapper.find((long)id);
    }

    @Override
    public List<Director> GetAll()
    {
        return directorMapper.findAll();
    }

    @Override
    public void save(Director item)
    {
       directorMapper.save(item);
    }

    @Override
    public void update(Director item)
    {
        directorMapper.update(item);
    }

    @Override
    public void remove(Director item)
    {
        directorMapper.remove(item);
    }
    
}
