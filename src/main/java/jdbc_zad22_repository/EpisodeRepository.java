/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import jdbc_zad22_dominanclass.Episode;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.EpisodeMapper;
import jdbc_zadv22_unityofwork.IUnitOfWork;

/**
 *
 * @author Grzegorz
 */
public class EpisodeRepository implements Repository<Episode>
{
    private EpisodeMapper episodeMapper;

    

    public EpisodeRepository(Connection connection, IUnitOfWork uow)
    {
        this.episodeMapper = new EpisodeMapper(connection, uow);
        

    }
    
    

    @Override
    public Episode GetById(int id)
    {
      return episodeMapper.find((long) id);
      
    }

    @Override
    public List<Episode> GetAll()
    {

        return episodeMapper.findAll();
    }

    @Override
    public void save(Episode item)
    {
        episodeMapper.save(item);
    }

    @Override
    public void update(Episode item)
    {
       episodeMapper.update(item);
    }

    @Override
    public void remove(Episode item)
    {
         episodeMapper.remove(item);
    }


}
