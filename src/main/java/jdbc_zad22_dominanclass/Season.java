/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_dominanclass;

import java.util.List;

/**
 *
 * @author Grzegorz
 */
public class Season extends Entity
{


    private int Id;
    private int seasonNumber;
    private int yearOfRealease;
    private List<Episode> episodes;
    private int IdTvSeries;

    public void setId(int ID)
    {
        this.Id = ID;
    }

    public int getSeasonNumber()
    {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber)
    {
        this.seasonNumber = seasonNumber;
    }

    public int getYearOfRealease()
    {
        return yearOfRealease;
    }

    public void setYearOfRealease(int yearOfRealease)
    {
        this.yearOfRealease = yearOfRealease;
    }

    public List<Episode> getEpisodes()
    {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes)
    {
        this.episodes = episodes;
    }

    public int getIdTvSeries()
    {
        return IdTvSeries;
    }

    public void setIdTvSeries(int IdTvSeries)
    {
        this.IdTvSeries = IdTvSeries;
    }

    @Override
    public int getId()
    {
        return this.Id;
    }

    @Override
    public String toString() {
        return "Season{" +
                "Id=" + Id +
                ", seasonNumber=" + seasonNumber +
                ", yearOfRealease=" + yearOfRealease +
                ", episodes=" + episodes +
                ", IdTvSeries=" + IdTvSeries +
                '}';
    }

}
