/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_dominanclass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grzegorz
 */
public class Director extends Entity
{

    private int Id;
    private String name;
    private Date dateofBirth;
    private String biography;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getDateofBirth()
    {
        return dateofBirth;
    }

    public void setDateofBirth(Date dateofBirth)
    {
        this.dateofBirth = dateofBirth;
    }

    public String getBiography()
    {
        return biography;
    }

    public void setBiography(String biography)
    {
        this.biography = biography;
    }

    public void setId(int Id)
    {
        this.Id = Id;
    }

    @Override
    public int getId()
    {
        return this.Id;
    }

	@Override
	public String toString() {
		return "Director [Id=" + Id + ", name=" + name + ", dateofBirth=" + dateofBirth + ", biography=" + biography
				+ "]";
	}
    
    

}
