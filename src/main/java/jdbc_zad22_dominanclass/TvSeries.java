/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_dominanclass;

import java.util.List;

/**
 *
 * @author Grzegorz
 */
public class TvSeries extends Entity
{
    private int Id;
    private String name;
    private List<Season> seasons;
    private int directorId;

    @Override
    public String toString()
    {
        return "TvSeries{" + "Id=" + Id + ", name=" + name + ", seasons=" + seasons + ", directorId=" + directorId + '}';
    }

    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Season> getSeasons()
    {
        return seasons;
    }

    public void setSeasons(List<Season> seasons)
    {
        this.seasons = seasons;
    }

    public int getDirectorId()
    {
        return directorId;
    }

    public void setDirectorId(int directorId)
    {
        this.directorId = directorId;
    }

   

    public void setId(int Id)
    {
        this.Id = Id;
    }


    
    @Override
    public int getId()
    {
       return this.Id;
    }
    
}
