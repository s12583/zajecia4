/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_dominanclass;

import java.time.Duration;
import java.util.Date;

/**
 *
 * @author Grzegorz
 */
public class Episode extends Entity
{

    private int Id;
    private String name;
    private Date realaseDate;
    private int episodeNumber;
    private Duration duration;
    private int id_Season;

    @Override
    public String toString()
    {
        return "Episode{" + "Id=" + Id + ", name=" + name + ", realaseDate=" + realaseDate + ", episodeNumber=" + episodeNumber + ", duration=" + duration + ", id_Season=" + id_Season + '}';
    }
    
    

    public int getId_Season()
    {
        return id_Season;
    }

    public void setId_Season(int id_Season)
    {
        this.id_Season = id_Season;
    }

   
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getRealaseDate()
    {
        return realaseDate;
    }

    public void setRealaseDate(Date realaseDate)
    {
       this.realaseDate = realaseDate;
    }

    public int getEpisodeNumber()
    {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber)
    {
        this.episodeNumber = episodeNumber;
    }

    public Duration getDuration()
    {
        return duration;
    }

    public void setDuration(Duration duration)
    {
        this.duration = duration;
    }

    public void setId(int Id)
    {
        this.Id = Id;
    }

    @Override
    public int getId()
    {
        return this.Id;
    }

}
