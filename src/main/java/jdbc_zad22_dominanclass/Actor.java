/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_dominanclass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grzegorz
 */
public class Actor extends Entity
{

    private int Id_TvSeries;
    private int Id;
    private String name;
    private Date dateofBirth;
    private String biography;

    

    public int getId_TvSeries()
    {
        return Id_TvSeries;
    }

    public void setId_TvSeries(int Id_TvSeries)
    {
        this.Id_TvSeries = Id_TvSeries;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getDateofBirth()
    {
        return dateofBirth;
    }

    public void setDateofBirth(Date dateofBirth)
    {
       this.dateofBirth = dateofBirth;

    }

    public String getBiography()
    {
        return biography;
    }

    public void setBiography(String biography)
    {
        this.biography = biography;
    }

    public void setId(int Id)
    {
        this.Id = Id;
    }

    @Override
    public int getId()
    {
        return this.Id;
    }
    
    @Override
    public String toString()
    {
        return "Actor{" + "Id_TvSeries=" + Id_TvSeries + ", Id=" + Id + ", name=" + name + ", dateofBirth=" + dateofBirth + ", biography=" + biography + '}';
    }

}
