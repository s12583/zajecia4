/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import jdbc_zad22_dominanclass.Episode;
import jdbc_zad22_dominanclass.Season;
import jdbc_zadv22_unityofwork.IUnitOfWork;

import static jdbc_zadv22_mappers.EpisodeMapper.FIND_STM_SEASON_ID;

/**
 *
 * @author Grzegorz
 */
public class SeasonMapper extends AbstractMapper<Season>
{
   private static final String COLUMNS = "id, seasonNumber,seasonYearOfRelease";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM Season WHERE id=?";
    public static final String FIND_ALL_STM = "SELECT " + COLUMNS + " FROM Season ";

    public static final String INSERT_STM = "INSERT INTO Season(seasonNumber,seasonYearOfRelease,idTvSeries) VALUES(?,?,?)";
    public static final String UPDATE_STM = "UPDATE Season SET seasonNumber =?,seasonYearOfRelease =?,idTvSeries =?  WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM Season WHERE id=?";
    public static final String FIND_STM_TVSERIES_ID = "SELECT " + COLUMNS + " FROM Season WHERE idTvSeries=?";

    private static final String seasonTable = "CREATE TABLE SEASON"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "seasonNumber INTEGER,"
            + "seasonYearOfRelease INTEGER,"
            + "idTvSeries INTEGER,"
            + "PRIMARY KEY ( id ))";
       
    public SeasonMapper(Connection connection, IUnitOfWork uow)
    {
        super(connection,uow);
        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("SEASON")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(seasonTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Season> allWithTvSeriesId (long id)
    {
        List <Season>  results = new ArrayList();

        PreparedStatement findStatement = null;

        try
        {
            findStatement = connection.prepareStatement(FIND_STM_TVSERIES_ID);
            findStatement.setInt(1, (int) id);

            ResultSet rs = findStatement.executeQuery();

            while (rs.next()) {
                Season result = load(rs);;
                results.add(result);
            }
            return results;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String findStatement()
    {
        return FIND_STM; 
    }

    @Override
    protected String findStatementAll() {
        return FIND_ALL_STM;
    }


    @Override
    protected String insertStatement()
    {
        return INSERT_STM; 
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM;
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Season doLoad(ResultSet rs) throws SQLException
    {
        Season season_tmp = new Season();
        season_tmp.setId(rs.getInt("id"));
        season_tmp.setSeasonNumber(rs.getInt("seasonNumber"));
        season_tmp.setYearOfRealease(rs.getInt("seasonYearOfRelease"));
        
        return season_tmp; 
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Season entity) throws SQLException
    {
        statement.setInt(1,entity.getSeasonNumber());
        statement.setInt(2,entity.getYearOfRealease());
        statement.setInt(3,entity.getIdTvSeries());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Season entity) throws SQLException
    {
        statement.setInt(1,entity.getSeasonNumber());
        statement.setInt(2,entity.getYearOfRealease());
        statement.setInt(3,entity.getIdTvSeries());
        statement.setLong(4,entity.getId());
    }
    
}
