/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import jdbc_zad22_dominanclass.Episode;
import jdbc_zadv22_unityofwork.IUnitOfWork;

/**
 *
 * @author Grzegorz
 */
public class EpisodeMapper extends AbstractMapper <Episode> 
{
    private static final String COLUMNS =     "id, episodeName,episodeReleaseDate,episodeNumber,episodeDuration";
    public static final String FIND_STM =     "SELECT " + COLUMNS + " FROM Episode WHERE id=?";
    public static final String FIND_ALL_STM = "SELECT " + COLUMNS + " FROM Episode ";
    public static final String INSERT_STM =   "INSERT INTO Episode(episodeName,episodeReleaseDate,episodeNumber,episodeDuration,idSeason) VALUES(?,?,?,?,?)";
    public static final String UPDATE_STM =   "UPDATE Episode SET episodeName = ?,episodeReleaseDate = ?,episodeNumber = ?, episodeDuration= ? WHERE id=?";
    public static final String DELETE_STM =   "DELETE FROM Episode WHERE id=?";
    public static final String FIND_STM_SEASON_ID = "SELECT " + COLUMNS + " FROM Episode WHERE idSeason=?";

    private static final String episodeTable = "CREATE TABLE EPISODE"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "episodeName VARCHAR(100),"
            + "episodeReleaseDate DATE,"
            + "episodeNumber INTEGER,"
            + "episodeDuration TIME,"
            + "idSeason INTEGER,"
            + "PRIMARY KEY ( id ))";


    //public static final String FIND_ALL_STM = "SELECT " + COLUMNS + " FROM Episode WHERE idSeason=?";
    // public static final String SELECT_BY_IDSEASON = "SELECT " + COLUMNS + " FROM Episode WHERE idSeason=?";


    public EpisodeMapper(Connection connection, IUnitOfWork uow)
    {
        super(connection,uow);

        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("EPISODE")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(episodeTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Episode> allWithSeasonId (long id)
    {
        List <Episode>  results = new ArrayList();

        PreparedStatement findStatement = null;

        try
        {
            findStatement = connection.prepareStatement(FIND_STM_SEASON_ID);
            findStatement.setInt(1, (int) id);

            ResultSet rs = findStatement.executeQuery();

            while (rs.next()) {
                Episode result = load(rs);
                results.add(result);
            }
            return results;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }



    @Override
    protected String findStatement()
    {
       return FIND_STM;
    }


    @Override
    protected String findStatementAll() {
       return FIND_ALL_STM;
    }


    @Override
    protected String insertStatement()
    {
        return INSERT_STM;
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM;
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Episode doLoad(ResultSet rs) throws SQLException
    {
      java.sql.Time sqlTime = rs.getTime("episodeDuration");
      LocalTime localTime = sqlTime.toLocalTime();      
      
              
        Episode episode_tmp = new Episode();
        episode_tmp.setId(rs.getInt("id"));
        episode_tmp.setName(rs.getString("episodeName"));
        episode_tmp.setDuration( Duration.between(LocalTime.MIDNIGHT, localTime));
        episode_tmp.setRealaseDate(rs.getDate("episodeReleaseDate"));
        episode_tmp.setEpisodeNumber(rs.getInt("episodeNumber"));
        return episode_tmp;  
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Episode entity) throws SQLException
    {
        LocalTime localTime = LocalTime.MIDNIGHT.plus(entity.getDuration());
        
        
        statement.setString(1,entity.getName());
        statement.setDate(2,new java.sql.Date(entity.getRealaseDate().getTime()));
        statement.setInt(3,entity.getEpisodeNumber());
        statement.setTime(4,java.sql.Time.valueOf(localTime));
        statement.setInt(5,entity.getId_Season());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Episode entity) throws SQLException
    {
        LocalTime localTime = LocalTime.MIDNIGHT.plus(entity.getDuration());

        statement.setString(1,entity.getName());
        statement.setDate(2,new java.sql.Date(entity.getRealaseDate().getTime()));
        statement.setInt(3,entity.getEpisodeNumber());
        statement.setTime(4,java.sql.Time.valueOf(localTime));
        statement.setInt(5,entity.getId_Season());
        statement.setLong(6,entity.getId());
    }


    
}
