/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;


import jdbc_zad22_dominanclass.Entity;
import jdbc_zadv22_unityofwork.IUnitOfWork;
import jdbc_zadv22_unityofwork.IUnitOfWorkMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractMapper<T extends Entity> implements IUnitOfWorkMapper<T> {

    protected Connection connection;
    protected IUnitOfWork uow;

    abstract protected String findStatement();

    abstract protected String findStatementAll();

    abstract protected String insertStatement();

    abstract protected String updateStatement();

    abstract protected String removeStatement();

    abstract protected T doLoad(ResultSet rs) throws SQLException;

    abstract protected void parametrizeInsertStatement(PreparedStatement statement, T entity) throws SQLException;

    abstract protected void parametrizeUpdateStatement(PreparedStatement statement, T entity) throws SQLException;

    private Map<Long, T> loadedMap = new HashMap<>();

    protected AbstractMapper(Connection connection, IUnitOfWork uow) {
        this.connection = connection;
        this.uow = uow;

    }

    public T find(Long id) {
        T result = loadedMap.get(id);

        if (result != null) {
            return result;
        }

        PreparedStatement findStatement = null;

        try {
            findStatement = connection.prepareStatement(findStatement());
            findStatement.setLong(1, id);
            ResultSet rs = findStatement.executeQuery();
            rs.next();

            result = load(rs);
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<T> findAll() {
        List<T> results = new ArrayList();

        PreparedStatement findStatement = null;

        try {
            findStatement = connection.prepareStatement(findStatementAll());

            ResultSet rs = findStatement.executeQuery();

            while (rs.next()) {
                T result = load(rs);
                ;
                results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    public void save(T entity) {

        uow.markAsNew(entity, this);


    }

    public void update(T entity) {

        uow.markAsDirty(entity, this);


    }

    public void remove(T entity) {

        uow.markAsDeleted(entity, this);


    }


    protected T load(ResultSet rs) throws SQLException {
        Long id = rs.getLong(1);
        if (loadedMap.containsKey(id)) {
            return loadedMap.get(id);
        }
        T result = doLoad(rs);
        loadedMap.put(id, result);
        return result;
    }

    private void clearMap(Long id) {
        if (loadedMap.containsKey(id)) {
            loadedMap.remove(id);
        }

    }


    @Override
    public void persistAdd(T entity) {

        PreparedStatement addStatement = null;
        try {
            addStatement = connection.prepareStatement(insertStatement());

            parametrizeInsertStatement(addStatement, entity);

            addStatement.executeUpdate();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void persistUpdate(T entity) {
        PreparedStatement updateStatement = null;
        try {
            updateStatement = connection.prepareStatement(updateStatement());

            parametrizeUpdateStatement(updateStatement, entity);

            updateStatement.executeUpdate();

            clearMap((long) entity.getId());


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void persistDelete(T entity) {
        PreparedStatement removeStatement = null;
        try {
            removeStatement = connection.prepareStatement(removeStatement());

            removeStatement.setLong(1, entity.getId());

            removeStatement.executeUpdate();

            clearMap((long) entity.getId());


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
