package jdbc_zad22_db_meneger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;


public class ConnectionDB
{
    private final String URL_DB = "jdbc:mysql://localhost:3306/tvseriesDB";
    private final String User = "root";
    private final String Password = "Drzewo456";
    private final String DriverName = "com.mysql.jdbc.Driver";
    private SQLWarning warn;
    private Connection con;
    
    
    public ConnectionDB()
    {
      try
      {
         Class.forName(DriverName); 
         this.con = DriverManager.getConnection(URL_DB,User,Password);
         if(this.con != null){ System.out.println("Połaczono z baza");}
         else {System.out.println("Problem z baza");} 
      }
      catch (Exception e)
      {
           e.printStackTrace(System.out);
           System.out.println("Problem z baza");
      }
            
    }
    
    public void closeConnection() throws SQLException
    {
        this.con.close();
    }

    public Connection getCon()
    {
        return con;
    }
    
        
    
}
