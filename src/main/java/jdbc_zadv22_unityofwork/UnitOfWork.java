
package jdbc_zadv22_unityofwork;

import jdbc_zad22_dominanclass.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;



public class UnitOfWork implements IUnitOfWork{

	
	private Connection connection;
	
	private Map<Entity, IUnitOfWorkMapper> entities = new LinkedHashMap<Entity, IUnitOfWorkMapper>();
	
	public UnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void commit() {

		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Changed:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case Unchanged:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void rollback() {

		entities.clear();
		
	}

	@Override
	public void markAsNew(Entity entity, IUnitOfWorkMapper repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);
		
	}

	@Override
	public void markAsDirty(Entity entity, IUnitOfWorkMapper repository) {
		entity.setState(EntityState.Changed);
		entities.put(entity, repository);
		
	}

	@Override
	public void markAsDeleted(Entity entity, IUnitOfWorkMapper repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
		
	}

}
