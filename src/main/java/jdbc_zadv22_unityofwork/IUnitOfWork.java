package jdbc_zadv22_unityofwork;

import jdbc_zad22_dominanclass.Entity;


public interface IUnitOfWork {

	public void commit();
	public void rollback();
	public void markAsNew(Entity entity, IUnitOfWorkMapper repository);
	public void markAsDirty(Entity entity, IUnitOfWorkMapper repository);
	public void markAsDeleted(Entity entity, IUnitOfWorkMapper repository);
}
