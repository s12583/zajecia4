package jdbc_zadv22_unityofwork;


import jdbc_zad22_dominanclass.Entity;

public interface IUnitOfWorkMapper <T>  {

	public void persistAdd(T entity);
	public void persistUpdate(T entity);
	public void persistDelete(T entity);
}
