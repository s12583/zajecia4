package jdbc_zadv22;


import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jdbc_zad22_db_meneger.ConnectionDB;
import jdbc_zad22_dominanclass.*;
import jdbc_zad22_repository.*;
import jdbc_zadv22_mappers.EpisodeMapper;
import jdbc_zadv22_mappers.SeasonMapper;
import jdbc_zadv22_unityofwork.UnitOfWork;

import javax.swing.text.TableView;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws SQLException {

        ConnectionDB con = new ConnectionDB();

        UnitOfWork un = new UnitOfWork(con.getCon());

        Date date = new Date(1994-07-12);

        Actor acor1 = new Actor();
        acor1.setBiography("dupa");
        acor1.setId_TvSeries(1);
        acor1.setName("fasdsad");
        acor1.setDateofBirth(date);
        //acor1.setId(1);

        Actor acor2 = new Actor();

        acor2.setBiography("drugi actor");
        acor2.setId_TvSeries(1);
        acor2.setName("snos");
        acor2.setDateofBirth(date);

        ActorRepository repoa = new ActorRepository(con.getCon(),un);

        //repoa.save(acor1);
        repoa.update(acor1);

       TvSeriesRepository reptv = new TvSeriesRepository(con.getCon(),un);



       SeasonRepository season = new SeasonRepository(con.getCon(),un);


        EpisodeMapper map = new EpisodeMapper(con.getCon(),un);

        map.allWithSeasonId(1);

        

       System.out.print(season.GetById(1));



    }
}
